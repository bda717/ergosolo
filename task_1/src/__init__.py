def get_data(file):
    with open(file, 'r') as file:
        return file.read().splitlines()


def join_data(data_id, data_int):
    for i in range(len(data_id)):
        try:
            yield data_id[i], data_int[i]
        except IndexError:
            yield data_id[i], None
