import sqlite3

from task_2.src import create_tables, initial_data, get_data

conn = sqlite3.connect('src/db.sqlite3')
db = conn.cursor()
try:
    create_tables(db)
    initial_data(db)
    get_data(db)
except Exception as e:
    print(e)
finally:
    db.close()
