sql_create_users = """
CREATE TABLE IF NOT EXISTS users (
id INTEGER PRIMARY KEY AUTOINCREMENT, 
name text TEXT
)
"""
users_initial = (("Buddy Rich",), ("Candido",), ("Charlie Byrd",))
sql_create_courses = """
CREATE TABLE IF NOT EXISTS courses (
id INTEGER PRIMARY KEY AUTOINCREMENT, 
name text TEXT
)
"""
courses_initial = (("Bachelor in Hotel Management",),
                   ("Bachelor of Architecture",),
                   ("Bachelor of Arts & Bachelor of Law",),
                   ("Bachelor of Arts Arabic (Honors)",),
                   ("Doctorate in Microbiology",),
                   ("Doctorate in Veterinary Science and Animal Husbandary",),
                   ("Bachelor of Architecture Interior Design",))

sql_create_saves = """
CREATE TABLE IF NOT EXISTS saves (
id INTEGER PRIMARY KEY AUTOINCREMENT, 
user_id INTEGER NOT NULL, 
course_id INTEGER NOT NULL, 
lesson_no TEXT, 
exercise_no TEXT, 
data TEXT,
CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES users(id),
CONSTRAINT fk_course_id FOREIGN KEY(course_id) REFERENCES courses(id)
);
CREATE INDEX IF NOT EXISTS i_user_id ON saves(user_id);
CREATE INDEX IF NOT EXISTS i_course_id ON saves(course_id);
"""
saves_initial = ((1, 1, "lesson_no", "exercise_no", "exercise_no"),
                 (2, 1, "lesson_no", "exercise_no", "exercise_no"),
                 (2, 1, "lesson_no", "exercise_no", "exercise_no"),
                 (2, 1, "lesson_no", "exercise_no", "exercise_no"),
                 (3, 1, "lesson_no", "exercise_no", "exercise_no"),
                 )

sql_result = """
SELECT users.name,  COUNT(*) AS exercise_count 
FROM saves 
JOIN users ON saves.user_id=users.id 
GROUP BY user_id
HAVING COUNT(*) = 3 
"""


def create_tables(db):
    db.execute(sql_create_users)
    db.execute(sql_create_courses)
    db.executescript(sql_create_saves)


def initial_data(db):
    db.executemany('INSERT INTO users (name) VALUES (?)', users_initial)
    # db.execute("SELECT * FROM users")
    # print(db.fetchall())

    db.executemany('INSERT INTO courses (name) VALUES (?)', courses_initial)
    # db.execute("SELECT * FROM courses")
    # print(db.fetchall())

    db.executemany('INSERT INTO saves (user_id, course_id, lesson_no, exercise_no, data) VALUES (?,?,?,?,?)',
                   saves_initial)
    # db.execute("SELECT * FROM saves")
    # print(db.fetchall())


def get_data(db):
    db.execute(sql_result)
    print(db.fetchall())
