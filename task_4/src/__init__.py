import csv
import itertools
import os


def get_data():
    domains = {}
    with open('%s/data.csv' % os.path.dirname(__file__), 'r') as file:
        content = csv.reader(file, delimiter=',')
        for item in content:
            try:
                domain = item[0].split('@')[1]
                domains[domain] = domains.get(domain, []) + [tuple(item)]
            except Exception as e:
                continue

    data = [tuple(filter(None, item)) for item in itertools.zip_longest(*domains.values())]
    return data
