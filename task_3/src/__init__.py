import xml.etree.ElementTree as ElementTree
from abc import ABC, abstractmethod

import memcache
import requests

__all__ = ('Memcached', 'CBR')


class CacheBase(ABC):
    @abstractmethod
    def set(self, *args, **kwargs):
        pass

    @abstractmethod
    def get(self, *args, **kwargs):
        pass


class Memcached(CacheBase):
    client = memcache.Client([('127.0.0.1', 11211)])

    def set(self, key, data, time=15):
        return self.client.set(key, data, time=time)

    def get(self, key):
        return self.client.get('currency')


class CBR:
    url = 'http://www.cbr.ru/scripts/XML_daily.asp'

    def __init__(self, cache=None):
        self.client = cache() if cache else None

    def get_data(self):
        response = requests.get(self.url)
        xml = ElementTree.fromstring(response.content)
        data = {}
        for item in xml:
            char_code = item.find('CharCode').text
            if char_code in ('USD', 'EUR'):
                data[char_code] = item.find('Value').text
        return data

    def get_cached_data(self, key):
        if self.client:
            data = self.client.get(key)
            if data:
                return data
            else:
                self.client.set(key, self.get_data())
                return self.client.get(key)
        else:
            print('Cache client is not defined')
